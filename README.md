[![nuget](https://img.shields.io/nuget/v/utest.net.svg?style=flat-square)](https://www.nuget.org/packages/utest.net)

# UTest.NET

Lightweight unit test framework for .NET

## Intro

It is wrapper over NUnit

The idea is having specific `input` directory for a test with its assets

A console output is redirected into a text file that it located in the `output` directory

The output directory is considered as unsafe sandbox for the test

The test is failed when the output directory is differed from `check` directory

## How to use it

See `utest.net.test` example

It tests the framework by means of the framework

The more advanced documentation is coming

Sorry, for inconvenience
