﻿using System;
using System.IO;

using NUnit.Framework;

namespace utest.net.test
{
	public class OtherTests : TestBase
	{
		[Test]
		public void ShouldRedirectConsoleOutput()
		{
			Console.WriteLine("Hello World!");
		}

		[Test]
		public void ShouldRunInProperFolder()
		{
			foreach (var entry in Directory.GetFileSystemEntries(".", "*", SearchOption.AllDirectories))
			{
				Console.WriteLine(entry);
			}
		}

		[Test]
		public void ShouldReadJsonFromCurrentDirectory()
		{
			object json = ReadJson<object>();

			WriteToJson(json);
		}
	}
}
