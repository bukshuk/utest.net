﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;

namespace utest.net.test
{
	public class InputFolderTests : TestBase
	{
		[Test]
		public void ShouldLeaveAllButConstFolder()
		{
		}

		[Test]
		public void ShouldCopyFromSiteConstFolder()
		{
			object data = ReadJsonFromConst<object>();

			WriteToJson(data);
		}

		[Test]
		public void ShouldCopyFromTestConstFolder()
		{
			object data = ReadJsonFromConst<object>();

			WriteToJson(data);
		}
	}
}
