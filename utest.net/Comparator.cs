﻿using System.IO;
using System.Linq;

namespace utest.net
{
	public class Comparator
    {
        public static bool FoldersAreEqual(string firstFolder, string secondFolder)
        {
            return Comparator.FoldersAreDifferent(firstFolder, secondFolder) == false;
        }

        public static bool FoldersAreDifferent(string firstFolder, string secondFolder)
        {
            if (Directory.Exists(firstFolder)==false)
            {
                throw new DirectoryNotFoundException(firstFolder);
            }
            if (Directory.Exists(secondFolder) == false)
            {
                throw new DirectoryNotFoundException(secondFolder);
            }

            var folderDiff = new FolderDiff.FolderDiff(firstFolder, secondFolder);

            return folderDiff.GetDiffs().Any(d => d.Name.StartsWith(".git") == false);
        }
    }
}
