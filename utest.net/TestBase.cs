﻿using System;
using System.Globalization;
using System.IO;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Threading;

using Newtonsoft.Json;

using NUnit.Framework;
using NUnit.Framework.Interfaces;

namespace utest.net
{
	[TestFixture]
	public class TestBase
	{
		protected const string JsonOutputFile = "output.json";

		protected const string JsonInputFile = "input.json";

		protected string TestOutputDirectory { get; private set; }

		protected string TestConstDirectory { get; private set; }

		[OneTimeSetUp]
		protected virtual void Init()
		{
			InitDirectory = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);

			ProjectDirectory = GetProjectDirectory(InitDirectory, 2);
		}

		[SetUp]
		protected virtual void TestInit()
		{
			if (string.IsNullOrWhiteSpace(ProjectDirectory))
			{
				return;
			}

			string testPath = GetTestPath();

			TestInputDirectory = Path.Combine(ProjectDirectory, InputDirectoryName, testPath);

			TestCheckDirectory = Path.Combine(ProjectDirectory, CheckDirectoryName, testPath);

			TestOutputDirectory = Path.Combine(InitDirectory, TestDirectoryName, testPath);

			TestConstDirectory = Path.Combine(TestOutputDirectory, ConstDirectoryName);

			var cd = Directory.GetCurrentDirectory();

			CreateDirectoryIfMissing(TestCheckDirectory);

			RecreateOutputDirectories();

			CopyInput();

			Directory.SetCurrentDirectory(TestOutputDirectory);

			RedirectConsoleOutput();

			Thread.CurrentThread.CurrentCulture = new CultureInfo("en-GB");
			Thread.CurrentThread.CurrentCulture.TextInfo.ListSeparator = ",";
		}

		[TearDown]
		protected virtual void TestCleanup()
		{
			if (string.IsNullOrWhiteSpace(ProjectDirectory))
			{
				return;
			}

			RestoreConsoleOutput();

			Directory.SetCurrentDirectory(InitDirectory);

			Folder.DeleteIfExists(TestConstDirectory);

			if (TestContext.CurrentContext.Result.Outcome.Status == TestStatus.Passed)
			{
				bool success = Comparator.FoldersAreEqual(TestCheckDirectory, TestOutputDirectory);
				if (success)
				{
					Thread.Sleep(100);
					Directory.Delete(TestOutputDirectory, true);
				}
				else
				{
					Assert.Fail("The output and control folders are differ");
				}
			}
		}

		[OneTimeTearDown]
		protected virtual void Cleanup()
		{
		}

		protected void WriteToJson(object objectToSerialize, string fileName = JsonOutputFile)
		{
			string jsonData = JsonConvert.SerializeObject(objectToSerialize, Formatting.Indented);

			File.WriteAllText(fileName, jsonData);
		}

		protected T ReadJson<T>(string fileName = JsonInputFile)
		{
			return DeserializeJson<T>(Path.Combine(TestOutputDirectory, fileName));
		}

		protected T ReadJsonFromConst<T>(string fileName = JsonInputFile)
		{
			return DeserializeJson<T>(Path.Combine(TestConstDirectory, fileName));
		}

		private T DeserializeJson<T>(string filePath)
		{
			T ret = default(T);

			if (File.Exists(filePath))
			{
				string jsonFileContent = File.ReadAllText(filePath);

				ret = JsonConvert.DeserializeObject<T>(jsonFileContent);
			}

			return ret;
		}

		private void RecreateOutputDirectories()
		{
			Folder.Recreate(TestOutputDirectory);

			Directory.CreateDirectory(TestConstDirectory);
		}

		private void CopyInput()
		{
			string siteInputPath = Path.GetDirectoryName(TestInputDirectory);

			string siteConstDirectory = Path.Combine(siteInputPath, ConstDirectoryName);

			if (Directory.Exists(siteConstDirectory))
			{
				CreateDirectoryIfMissing(TestConstDirectory);

				FS.CopyAllButHidden(siteConstDirectory, TestConstDirectory);
			}

			if (Directory.Exists(TestInputDirectory))
			{
				FS.CopyAllButHidden(TestInputDirectory, TestOutputDirectory);
			}
		}

		private void RedirectConsoleOutput()
		{
			ConsoleWriter = new StreamWriter(ConsoleOutputFile);

			Console.SetError(ConsoleWriter);

			Console.SetOut(ConsoleWriter);
		}

		private void RestoreConsoleOutput()
		{
			ConsoleWriter.Close();

			RemoveOutputFileIfEmpty();

			Console.SetOut(new StreamWriter(Console.OpenStandardOutput()));

			Console.SetError(new StreamWriter(Console.OpenStandardError()));
		}

		private string GetProjectDirectory(string baseDirectory, byte depth)
		{
			if (Directory.Exists(Path.Combine(baseDirectory, CheckDirectoryName)))
			{
				return baseDirectory;
			}

			if (depth > 0)
			{
				return GetProjectDirectory(Path.GetDirectoryName(baseDirectory), --depth);
			}

			return string.Empty;
		}

		private string GetTestPath()
		{
			var match = Regex.Match(TestContext.CurrentContext.Test.FullName, @"\.([^.]+)\.([^.]+)$");

			string siteName = match.Groups[1].Value;
			string testName = match.Groups[2].Value;

			return Path.Combine(siteName, testName);
		}

		private void RemoveOutputFileIfEmpty()
		{
			if (File.Exists(ConsoleOutputFile))
			{
				if (string.IsNullOrWhiteSpace(File.ReadAllText(ConsoleOutputFile)))
				{
					File.Delete(ConsoleOutputFile);
				}
			}
		}

		private void CreateDirectoryIfMissing(string directory)
		{
			if (!Directory.Exists(directory))
			{
				Directory.CreateDirectory(directory);
			}
		}


		const string TestDirectoryName = "test";

		const string ConstDirectoryName = "const";

		const string InputDirectoryName = "input";

		const string CheckDirectoryName = "check";

		const string ConsoleOutputFile = "console.txt";


		string InitDirectory { get; set; }

		string ProjectDirectory { get; set; }

		string TestInputDirectory { get; set; }

		string TestCheckDirectory { get; set; }


		StreamWriter ConsoleWriter { get; set; }
	}
}
