﻿using System.Diagnostics;
using System.IO;
using static Microsoft.VisualBasic.FileIO.FileSystem;

namespace utest.net
{
    public class FS
    {
        public static void CopyAllButHidden(string a_What, string a_Where)
        {
            foreach (string file in Directory.GetFiles(a_What))
            {
                if ((File.GetAttributes(file) & FileAttributes.Hidden) != FileAttributes.Hidden)
                {
                    File.Copy(file, BPath.CombinePathWithFile(a_Where, file), true);
                }
            }

            foreach (string directory in Directory.GetDirectories(a_What))
            {
                if ((File.GetAttributes(directory) & FileAttributes.Hidden) != FileAttributes.Hidden)
                {
                    Directory.CreateDirectory(BPath.CombinePathWithFile(a_Where, directory));

                    FS.CopyAllButHidden(directory, BPath.CombinePathWithFile(a_Where, directory));
                }
            }
        }

        public static void Copy(string a_What, string a_Where)
        {
            string rootDirectory = Path.GetDirectoryName(a_What);

            if (string.IsNullOrEmpty(rootDirectory))
            {
                rootDirectory = Directory.GetCurrentDirectory();
            }

            if (Directory.Exists(rootDirectory))
            {
                string pattern = Path.GetFileName(a_What);

                foreach (string directory in Directory.GetDirectories(rootDirectory, pattern))
                {
                    CopyDirectory(directory, BPath.CombinePathWithFile(a_Where, directory), true);
                }

                string where = a_Where;

                foreach (string file in Directory.GetFiles(rootDirectory, pattern))
                {
                    if (Directory.Exists(a_Where))
                    {
                        where = BPath.CombinePathWithFile(a_Where, file);
                    }

                    CopyFile(file, where, true);
                }
            }
        }

        public static void Remove(string a_What)
        {
            string rootDirectory = Path.GetDirectoryName(BPath.RemoveEndSeparators(a_What));

            if (string.IsNullOrEmpty(rootDirectory))
            {
                rootDirectory = Directory.GetCurrentDirectory();
            }

            if (Directory.Exists(rootDirectory))
            {
                string pattern = Path.GetFileName(BPath.RemoveEndSeparators(a_What));

                foreach (string dir in Directory.GetDirectories(rootDirectory, pattern, SearchOption.TopDirectoryOnly))
                {
                    RemoveDirectoryMsdos(dir);
                }

                foreach (string file in Directory.GetFiles(rootDirectory, pattern, SearchOption.TopDirectoryOnly))
                {
                    File.SetAttributes(file, FileAttributes.Normal);

                    File.Delete(file);
                }
            }
        }

        public static string Backup(string aFilePath)
        {
            string backupPath = string.Empty;

            if (!File.Exists(aFilePath))
            {
                return backupPath;
            }

            int index = 0;

            do
            {
                backupPath = string.Format("{0}.b{1}", aFilePath, index);
                ++index;
            }
            while (File.Exists(backupPath));

            File.Copy(aFilePath, backupPath);

            return backupPath;
        }

        public static void RemoveDirectoryMsdos(string a_Directory)
        {
            ProcessViaCmd(a_Directory, "rmdir /S /Q");
        }

        public static void RemoveFileMsdos(string a_File)
        {
            ProcessViaCmd(a_File, "del /F /S /Q");
        }

        private static void ProcessViaCmd(string a_Path, string a_MsdosCommand)
        {
            using (Process process = new Process())
            {
                process.StartInfo.FileName = "cmd";
                process.StartInfo.Arguments = string.Format(@"/C {0} ""{1}""", a_MsdosCommand, a_Path);

                process.StartInfo.UseShellExecute = false;
                process.StartInfo.RedirectStandardOutput = true;

                process.OutputDataReceived += delegate
                {
                };

                process.Start();

                process.BeginOutputReadLine();

                process.WaitForExit();
            }
        }
    }
}
