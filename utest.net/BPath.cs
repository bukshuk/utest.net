﻿using System.IO;
using System.Text.RegularExpressions;

namespace utest.net
{
    public class BPath
    {
        private static string DirectorySeparator
        {
            get
            {
                return Path.DirectorySeparatorChar.ToString();
            }
        }

        public static string RemoveEndSeparators(string a_Path)
        {
            return Regex.Replace(a_Path, @"^(.*[:\w])[\\/]+$", "$1");
        }

        public static string RemoveBeginSeparators(string a_Path)
        {
            return Regex.Replace(a_Path, @"^[\\/]+(\w.*)$", "$1");
        }

        public static string GetFileName(string a_Path, bool a_WithExtention)
        {
            if (a_WithExtention)
            {
                return Path.GetFileName(a_Path);
            }

            return Path.GetFileNameWithoutExtension(a_Path);
        }

        public static string GetExtention(string a_Path)
        {
            return Path.GetExtension(a_Path);
        }

        public static string GetDirectoryPath(string a_Path)
        {
            return Path.GetDirectoryName(a_Path);
        }

        public static string GetFullPath(string a_Path)
        {
            return Path.GetFullPath(a_Path);
        }

        public static string GetDriveName(string aPath)
        {
            string fullPath = GetFullPath(aPath);

            Match match = Regex.Match(aPath, @"^(\w):");

            if (match.Success)
            {
                return match.Groups[1].Value;
            }

            return string.Empty;
        }

        public static string CombinePathWithFile(string aPath, string aFilePath)
        {
            return BPath.RemoveEndSeparators(aPath) + BPath.DirectorySeparator + BPath.GetFileName(aFilePath, true);
        }

        public static string CombinePathWithPath(string aPath, string aAddPath)
        {
            return BPath.RemoveEndSeparators(aPath) + BPath.DirectorySeparator + BPath.RemoveBeginSeparators(aAddPath);
        }

        public static string ToUnix(string a_Path)
        {
            string str = Regex.Replace(a_Path, "^(\\w):", "/$1");

            return Regex.Replace(str, "\\\\", "/");
        }

        public static string ToWindows(string a_Path)
        {
            string str = Regex.Replace(a_Path, "^/(\\w)/", "$1:/");

            return Regex.Replace(str, "/", "\\\\");
        }

        public static string ToWindowsDoubleSlashes(string a_Path)
        {
            string str = BPath.ToWindows(a_Path);

            return Regex.Replace(str, "\\\\", "\\\\\\\\");
        }
    }
}
