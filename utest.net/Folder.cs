﻿using System.IO;
using System.Linq;

namespace utest.net
{
    public static class Folder
    {
        public static void Recreate(string a_Path)
        {
            if (Directory.Exists(a_Path))
            {
                Directory.Delete(a_Path, true);
            }

            Directory.CreateDirectory(a_Path);
        }

        public static void DeleteIfEmpty(string a_Path)
        {
            if (Directory.GetFileSystemEntries(a_Path).Count() == 0)
            {
                Directory.Delete(a_Path, false);
            }
        }

        public static void DeleteIfExists(string a_Path)
        {
            if (Directory.Exists(a_Path))
            {
                Directory.Delete(a_Path, true);
            }
        }
    }
}
